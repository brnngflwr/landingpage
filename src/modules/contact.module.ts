export interface Contact {
    // for now optional properties
    fName: string;
    lName: string;
    email: string;
    quest: string;
}
