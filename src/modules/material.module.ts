// Material Design Components Imports
// Import through here all needed Material components
// https://material.angular.io/components/categories

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Forms Control
import { MatFormFieldModule, MatFormFieldControl, MatInputModule, MatRadioModule, MatSliderModule } from '@angular/material';

// Navigation
import { MatToolbarModule, MatMenuModule } from '@angular/material';

// Layout
import { MatCardModule, MatExpansionModule, MatTabsModule } from '@angular/material';

// Buttons & Indications
import { MatIconModule, MatButtonModule, MatProgressBarModule } from '@angular/material';

// Popups
import {} from '';

// Data table
import {} from '';

@NgModule({
  imports: [
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSliderModule,
    MatExpansionModule,
    MatTabsModule
  ],
  exports: [
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSliderModule,
    MatExpansionModule,
    MatTabsModule
  ],
})
export class MaterialModule { }
