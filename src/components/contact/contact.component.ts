import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contact: FormGroup;
  ngOnInit() {
    this.contact = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(3)
      ]),
      last: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(3),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      quest: new FormControl('', [
        Validators.minLength(0),
        Validators.maxLength(258)
      ])
    });
  }

  nameErrorMessage() {
    const errs = this.contact.controls ;
    return errs.name.hasError('required') ? 'Nombre requerido' : errs.name.hasError('pattern') ? 'Ingrese nombre valido' : '';
  }

  lastErrorMessage() {
    const errs = this.contact.controls ;
    return errs.last.hasError('required') ? 'Apellido requerido' : errs.last.hasError('pattern') ? 'Ingrese apellido valido' : '';
    // console.log(errs);
  }

  emailErrorMessage() {
    const errs = this.contact.controls ;
    return errs.email.hasError('required') ? 'Email requerido' : errs.email.hasError('email') ? 'Ingrese email valido' : '';
    // console.log(errs);
  }

  questErrorMessage() {
    const errs = this.contact.controls ;
    return errs.quest.hasError('maxLength') ? 'Ha exedido el limite de caracteres' : '';
    // console.log(errs);
  }
  onSubmit(obj) {
    console.log(obj.value);
  }
}
