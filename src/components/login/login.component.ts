import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: FormGroup;
  ngOnInit() {
    this.login = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$'),
        Validators.minLength(3)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      passw: new FormControl('', [
        Validators.minLength(0),
        Validators.maxLength(258)
      ])
    });
  }

  nameErrorMessage() {
    const errs = this.login.controls ;
    return errs.name.hasError('required') ? 'Nombre requerido' : errs.name.hasError('pattern') ? 'Ingrese nombre valido' : '';
  }

  emailErrorMessage() {
    const errs = this.login.controls ;
    return errs.email.hasError('required') ? 'Email requerido' : errs.email.hasError('email') ? 'Ingrese email valido' : '';
    // console.log(errs);
  }

  passwErrorMessage() {
    const errs = this.login.controls ;
    return errs.passw.hasError('maxLength') ? 'Ha exedido el limite de caracteres' : '';
    // console.log(errs);
  }
  onSubmit(obj) {
    console.log(obj.value);
  }
}
